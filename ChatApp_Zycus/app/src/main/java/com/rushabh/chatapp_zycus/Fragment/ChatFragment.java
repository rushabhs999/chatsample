package com.rushabh.chatapp_zycus.Fragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.rushabh.chatapp_zycus.Adapter.ChatAdapter;
import com.rushabh.chatapp_zycus.Model.ChatModel;
import com.rushabh.chatapp_zycus.R;
import com.rushabh.chatapp_zycus.Utils.TimeConversionUtils;
import com.rushabh.chatapp_zycus.Utils.UIUtils;
import com.rushabh.chatapp_zycus.Utils.URLConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChatFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChatFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChatFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private String TAG = getClass().getName();
    private Activity activity;

    private RecyclerView rv_chat;
    private TextView tv_message;
    private ProgressBar progress_circular;
    private ArrayList<ChatModel> chatModelArrayList;
    private ChatAdapter chatAdapter;

    public ChatFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChatFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChatFragment newInstance(String param1, String param2) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        initViews(view);
        return view;
    }

    public void initViews(View view) {
        activity = getActivity();
        rv_chat = view.findViewById(R.id.rv_chat);
        tv_message = view.findViewById(R.id.tv_message);
        progress_circular = view.findViewById(R.id.progress_circular);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        rv_chat.setHasFixedSize(false);
        rv_chat.setNestedScrollingEnabled(false);
        rv_chat.setLayoutManager(linearLayoutManager);
        chatModelArrayList = new ArrayList<>();
        chatAdapter = new ChatAdapter(activity, chatModelArrayList);
        rv_chat.setAdapter(chatAdapter);


        getMessages();
    }


    public void getMessages() {
        try {
            if (UIUtils.isNetworkAvailable(activity)) {
                showProgressDialog();
                try {
                    final String URL = URLConstants.CHAT;
                    Log.d(TAG, "getMessages " + URL);

                    // Initialize a new RequestQueue instance
                    RequestQueue requestQueue = Volley.newRequestQueue(activity);

                    // Initialize a new JsonObjectRequest instance
                    JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null,
                            new Response.Listener<JSONArray>() {
                                @Override
                                public void onResponse(JSONArray response) {
                                    // Do something with response
                                    try {
                                        Log.d(TAG, "getMessages  response " + response.toString());
                                        clearMessagesLists();

                                        for (int i = 0; i < response.length(); i++) {
                                            JSONObject jsonObject = response.getJSONObject(i);
                                            Long messageId = jsonObject.has("messageId") ? jsonObject.getLong("messageId") : 0;
                                            Long createdDate = jsonObject.has("createdDate") ? jsonObject.getLong("createdDate") : 0;
                                            String messageText = jsonObject.has("messageText") ? jsonObject.getString("messageText") : "";
                                            String senderName = jsonObject.has("senderName") ? jsonObject.getString("senderName") : "";
                                            Boolean isSender = jsonObject.has("isSender") ? jsonObject.getBoolean("isSender") : false;

                                            Date newDate = new Date();

                                            chatModelArrayList.add(new ChatModel(messageId, createdDate, messageText, senderName, isSender, newDate));

                                        }

                                        chatAdapter.notifyDataSetChanged();

                                        if (chatModelArrayList.size() == 0) {
                                            tv_message.setText(R.string.no_messages);
                                            tv_message.setVisibility(View.VISIBLE);
                                        } else {
                                            tv_message.setVisibility(View.GONE);
                                        }
                                        dismissProgressDialog();


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    try {
                                        //Do something when error occurred
                                        NetworkResponse networkResponse = error.networkResponse;
//                                        if (networkResponse != null && networkResponse.statusCode == 403) {
//                                            UIUtils.sessionAlert(activity);
//                                        } else if (networkResponse != null && networkResponse.statusCode == 401) {
//                                            UIUtils.sessionAlert(activity);
//                                        } else
                                        if (networkResponse != null && networkResponse.statusCode == 500) {
                                            tv_message.setText(R.string.some_error_occurrred);
                                            tv_message.setVisibility(View.VISIBLE);
                                        } else {
                                            Log.d(TAG, activity.getString(R.string.some_error_occurrred));
                                            tv_message.setText(R.string.some_error_occurrred);
                                            tv_message.setVisibility(View.VISIBLE);
                                        }
                                        dismissProgressDialog();
                                    } catch (Exception e) {
                                        dismissProgressDialog();
                                        e.printStackTrace();
                                    }
                                }
                            }
                    );
                    requestQueue.add(jsonArrayRequest);
                    //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions.
                    //Volley does retry for you if you have specified the policy.
                    jsonArrayRequest.setRetryPolicy(new

                            DefaultRetryPolicy(5000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                } catch (Exception e) {
                    e.printStackTrace();

                }
            } else {
                Toast.makeText(activity, getResources().getString(R.string.msg_please_check_internet_connection), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void clearMessagesLists() {
        chatModelArrayList.clear();
    }

    private void dismissProgressDialog() {
        progress_circular.setVisibility(View.GONE);
    }

    private void showProgressDialog() {
        progress_circular.setVisibility(View.VISIBLE);
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
