package com.rushabh.chatapp_zycus.Model;

import java.util.Date;

public class ChatModel {


    public Long messageId,createdDate;
    public String messageText,senderName;
    public boolean isSender ;
    public Date convertedDate;


    public ChatModel(Long messageId, Long createdDate, String messageText, String senderName, boolean isSender, Date convertedDate) {
        this.messageId = messageId;
        this.createdDate = createdDate;
        this.messageText = messageText;
        this.senderName = senderName;
        this.isSender = isSender;
        this.convertedDate = convertedDate;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public boolean isSender() {
        return isSender;
    }

    public void setSender(boolean sender) {
        isSender = sender;
    }

    public Date getConvertedDate() {
        return convertedDate;
    }

    public void setConvertedDate(Date convertedDate) {
        this.convertedDate = convertedDate;
    }
}
