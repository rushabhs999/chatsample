package com.rushabh.chatapp_zycus.Utils;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class TimeConversionUtils {

    public static String TAG = TimeConversionUtils.class.getName();

    public static Date longToDate(Long timestamp) {
        Date convertedDate = new Date();
        try {
            convertedDate = new Date(timestamp);
            Log.d(TAG, "convertd_MMM_y " + convertedDate);

            return convertedDate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertedDate;
    }

    public static String convertd_d_MMM_y(String timestamp) {
        String formattedDate = "";
        try {
            String value = timestamp;
            String length = value.substring(0, value.length() - 1);
            String replaceTo = length.replace("T", " ");
            System.out.println("newVal>" + replaceTo);

            DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd h:m:S", Locale.ENGLISH);
            DateFormat targetFormat = new SimpleDateFormat("d-MMM-y");
            Date date = originalFormat.parse(replaceTo);
            //Date date = originalFormat.parse("2017-12-18 4:58:30.000");
            formattedDate = targetFormat.format(date);
            System.out.println("DATE>>" + formattedDate);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String timestampToDateString(long timestamp){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date(timestamp);
        return dateFormat.format(date);
    }
}
