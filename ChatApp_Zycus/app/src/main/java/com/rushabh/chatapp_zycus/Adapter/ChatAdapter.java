package com.rushabh.chatapp_zycus.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rushabh.chatapp_zycus.Model.ChatModel;
import com.rushabh.chatapp_zycus.R;
import com.rushabh.chatapp_zycus.Utils.TimeConversionUtils;

import java.util.ArrayList;
import java.util.Date;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity activity;
    private ArrayList<ChatModel> chatModelArrayList;

    public static final int VIEW_TYPE_SENDER = 1;
    public static final int VIEW_TYPE_USER = 2;
    private String TAG = getClass().getName();

    public ChatAdapter(Activity activity, ArrayList<ChatModel> chatModelArrayList) {
        this.activity = activity;
        this.chatModelArrayList = chatModelArrayList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view;
        if (viewType == VIEW_TYPE_SENDER) {
            view = LayoutInflater.from(activity).inflate(R.layout.item_sender, viewGroup, false);
            return new SenderViewHolder(view);
        } else if (viewType == VIEW_TYPE_USER) {
            view = LayoutInflater.from(activity).inflate(R.layout.item_user, viewGroup, false);
            return new UserViewHolder(view);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {

        ChatModel chatModel = chatModelArrayList.get(position);

        Log.d(TAG, "chatModel.isSender " + position + "  " + chatModel.isSender);
        if (chatModel.isSender) {
            return VIEW_TYPE_SENDER;
        } else {
            return VIEW_TYPE_USER;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ChatModel chatModel = chatModelArrayList.get(position);
        switch (getItemViewType(position)) {
            case VIEW_TYPE_SENDER:
                ((SenderViewHolder) viewHolder).bind(chatModel);
                break;
            case VIEW_TYPE_USER:
                ((UserViewHolder) viewHolder).bind(chatModel);
        }
    }

    @Override
    public int getItemCount() {
        return chatModelArrayList.size();
    }

    public class SenderViewHolder extends RecyclerView.ViewHolder {
        LinearLayout ll_sender;
        TextView tv_sender, tv_senderTime;

        public SenderViewHolder(@NonNull View itemView) {
            super(itemView);
            ll_sender = itemView.findViewById(R.id.ll_sender);
            tv_sender = itemView.findViewById(R.id.tv_sender);
            tv_senderTime = itemView.findViewById(R.id.tv_senderTime);
        }

        public void bind(ChatModel chatModel) {
            Long messageId = chatModel.messageId;
            Long createdDate = chatModel.createdDate;
            String senderName = chatModel.senderName;
            String messageText = chatModel.messageText;
            Boolean isSender = chatModel.isSender;

            tv_sender.setText(messageText);
            tv_senderTime.setText(senderName + " " + TimeConversionUtils.timestampToDateString(createdDate));
        }
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {
        LinearLayout ll_user;
        TextView tv_user, tv_userTime;

        public UserViewHolder(@NonNull View itemView) {
            super(itemView);
            ll_user = itemView.findViewById(R.id.ll_user);
            tv_user = itemView.findViewById(R.id.tv_user);
            tv_userTime = itemView.findViewById(R.id.tv_userTime);
        }

        public void bind(ChatModel chatModel) {
            Long messageId = chatModel.messageId;
            Long createdDate = chatModel.createdDate;
            String senderName = chatModel.senderName;
            String messageText = chatModel.messageText;
            Boolean isSender = chatModel.isSender;
            Date convertedDate = chatModel.convertedDate;

            tv_user.setText(messageText);
            System.out.println("User createdDate " + createdDate + " convertedDate " + convertedDate);
            tv_userTime.setText(senderName + " " + TimeConversionUtils.timestampToDateString(createdDate));

        }
    }
}
