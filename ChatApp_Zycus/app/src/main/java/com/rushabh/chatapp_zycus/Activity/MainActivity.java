package com.rushabh.chatapp_zycus.Activity;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.rushabh.chatapp_zycus.Fragment.ChatFragment;
import com.rushabh.chatapp_zycus.R;

public class MainActivity extends AppCompatActivity implements ChatFragment.OnFragmentInteractionListener {

    private Activity activity;
    private String TAG = getClass().getName();

    private FrameLayout frame_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
    }

    private void initViews(){
        activity = MainActivity.this;
        frame_content = findViewById(R.id.frame_content);

        setFragment(new ChatFragment());
    }


    private void setFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame_content, fragment);
        ft.commit();
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
